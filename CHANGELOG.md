# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.33.0](https://gitlab.com/ensembl/dev1on1/compare/v1.32.0...v1.33.0) (2021-07-14)


### Features

* **Setup:** Use real name instead of display name ([be6618b](https://gitlab.com/ensembl/dev1on1/commit/be6618bd7c99f4f6575e6a4fad2335474e3d3621))

## [1.32.0](https://gitlab.com/ensembl/dev1on1/compare/v1.31.0...v1.32.0) (2021-07-14)


### Features

* **setup:** Use conversations.members api to get DM members ([a90bf2f](https://gitlab.com/ensembl/dev1on1/commit/a90bf2f034b093eba1e90097dd134a4260529013))

## [1.31.0](https://gitlab.com/ensembl/dev1on1/compare/v1.30.0...v1.31.0) (2021-07-13)


### Features

* **Survey:** Larger free text entry box ([8ae2ec3](https://gitlab.com/ensembl/dev1on1/commit/8ae2ec3c488d504aa1501600f6f61201b8830e7d))

## [1.30.0](https://gitlab.com/ensembl/dev1on1/compare/v1.29.1...v1.30.0) (2021-07-13)


### Features

* **reminder:** Dedupe ICs in survey reminders ([f5ef431](https://gitlab.com/ensembl/dev1on1/commit/f5ef43183b21bdff12bfc51872c77f72de1d1aca))

### [1.29.1](https://gitlab.com/ensembl/dev1on1/compare/v1.29.0...v1.29.1) (2021-07-13)


### Bug Fixes

* **Reminder:** Fix to look at channels to find ICs ([fd7684e](https://gitlab.com/ensembl/dev1on1/commit/fd7684e3829b0130c81093a013a57bc89ffe1d2b))

## [1.29.0](https://gitlab.com/ensembl/dev1on1/compare/v1.28.0...v1.29.0) (2021-07-13)


### Features

* **oAuth:** Create team document on oAuth ([f443c35](https://gitlab.com/ensembl/dev1on1/commit/f443c35e6d47db1bbbe06934aba37ffd20052a70))

## [1.28.0](https://gitlab.com/ensembl/dev1on1/compare/v1.27.0...v1.28.0) (2021-07-13)


### Features

* **Report:** Show user name in report and customize no submissions yet message to user role ([84b7fe0](https://gitlab.com/ensembl/dev1on1/commit/84b7fe0cd6a7fc59bbecbce05115af108e203974))

## [1.27.0](https://gitlab.com/ensembl/dev1on1/compare/v1.26.0...v1.27.0) (2021-07-13)


### Features

* **report:** Fix report to read from team-rooted collection ([98f0314](https://gitlab.com/ensembl/dev1on1/commit/98f0314ba0b56c69d6dd3de7fec39f9ba98e5445))

## [1.26.0](https://gitlab.com/ensembl/dev1on1/compare/v1.25.0...v1.26.0) (2021-07-13)


### Features

* **Survey:** Write submissions to team rooted collection ([d298b98](https://gitlab.com/ensembl/dev1on1/commit/d298b9800a9787e04788c633418bf58d3a928bb7))

## [1.25.0](https://gitlab.com/ensembl/dev1on1/compare/v1.24.0...v1.25.0) (2021-07-13)


### Features

* **Set up channel:** Before survey, require set up ([09d0fd9](https://gitlab.com/ensembl/dev1on1/commit/09d0fd93570bd6bcc506c98f12aa6e0cce622d67))

## [1.24.0](https://gitlab.com/ensembl/dev1on1/compare/v1.23.0...v1.24.0) (2021-07-13)


### Features

* **setup:** Setup changed message ([e94298c](https://gitlab.com/ensembl/dev1on1/commit/e94298cc125d08488eaf8dcc0d0e27bb08f25100))

## [1.23.0](https://gitlab.com/ensembl/dev1on1/compare/v1.22.0...v1.23.0) (2021-07-13)


### Features

* **setup:** Show different message and buttons depending ([3f745e9](https://gitlab.com/ensembl/dev1on1/commit/3f745e9d2cf0b802748c05d0ffd3ee46f5c99538))

## [1.22.0](https://gitlab.com/ensembl/dev1on1/compare/v1.21.0...v1.22.0) (2021-07-13)


### Features

* **Store roles:** Feedback message after store roles ([46d8679](https://gitlab.com/ensembl/dev1on1/commit/46d8679fccc6df543427095b24ff647ce16e1333))
* **Store roles:** Feedback message after store roles ([6fbed6c](https://gitlab.com/ensembl/dev1on1/commit/6fbed6cec1ba1b9f7107958e90c5de63036789f0))

## [1.21.0](https://gitlab.com/ensembl/dev1on1/compare/v1.20.0...v1.21.0) (2021-07-12)


### Features

* **setup:** Store IC and Manager info ([807edd5](https://gitlab.com/ensembl/dev1on1/commit/807edd59e08b8caac017bc5169e58dff92e04583))

## [1.20.0](https://gitlab.com/ensembl/dev1on1/compare/v1.19.0...v1.20.0) (2021-07-12)


### Features

* **setup:** Buttons for setup command ([fcf1f7e](https://gitlab.com/ensembl/dev1on1/commit/fcf1f7e6dfb671a67260174ca6ae0c7d1bb4aef4))

## [1.19.0](https://gitlab.com/ensembl/dev1on1/compare/v1.18.0...v1.19.0) (2021-07-12)


### Features

* **report:** Generate report with new Firestore arch ([49c997a](https://gitlab.com/ensembl/dev1on1/commit/49c997adec3baa70afeed43c0c5a15b7288a86f4))

## [1.18.0](https://gitlab.com/ensembl/dev1on1/compare/v1.17.0...v1.18.0) (2021-07-12)


### Features

* **Survey submit:** Submit survey data according to new Firestore arch ([500c069](https://gitlab.com/ensembl/dev1on1/commit/500c069b4a3cbcebff0fdabb71134d41b0ee88cd))

### [1.17.1](https://gitlab.com/ensembl/dev1on1/compare/v1.17.0...v1.17.1) (2021-07-12)

## [1.17.0](https://gitlab.com/ensembl/dev1on1/compare/v1.16.0...v1.17.0) (2021-07-12)


### Features

* **setup slash command:** Get names of users in channel ([a0bfb10](https://gitlab.com/ensembl/dev1on1/commit/a0bfb1027634717747e36241e75e82ebced897de))
* **survey:** Feedback in modal after submission ([165acbb](https://gitlab.com/ensembl/dev1on1/commit/165acbb03c6587af0b84e279722be3f03cc05e77))

## [1.16.0](https://gitlab.com/ensembl/dev1on1/compare/v1.15.0...v1.16.0) (2021-07-12)


### Features

* **setup:** Show setup message for set up slash command ([ed5a1a0](https://gitlab.com/ensembl/dev1on1/commit/ed5a1a00e93b2dbeffdf9856ad902add9f9d07ab))

## [1.15.0](https://gitlab.com/ensembl/dev1on1/compare/v1.14.0...v1.15.0) (2021-07-12)


### Features

* Change slash command wording ([06f6c43](https://gitlab.com/ensembl/dev1on1/commit/06f6c43efa9711ac5bcda5c83b7edd5d9796feb4))

## [1.14.0](https://gitlab.com/ensembl/dev1on1/compare/v1.13.1...v1.14.0) (2021-07-10)


### Features

* Update onborading message ([456d9c0](https://gitlab.com/ensembl/dev1on1/commit/456d9c0b136d14697c8298093099f7a129897579))

### [1.13.1](https://gitlab.com/ensembl/dev1on1/compare/v1.13.0...v1.13.1) (2021-07-10)

## [1.13.0](https://gitlab.com/ensembl/dev1on1/compare/v1.12.1...v1.13.0) (2021-07-10)


### Features

* **Survey submission:** Send feedback success message ([c96fe5c](https://gitlab.com/ensembl/dev1on1/commit/c96fe5ca42c58c56362da0e84066a46cf52db5d8))

### [1.12.1](https://gitlab.com/ensembl/dev1on1/compare/v1.12.0...v1.12.1) (2021-07-10)

## [1.12.0](https://gitlab.com/ensembl/dev1on1/compare/v1.11.1...v1.12.0) (2021-07-10)


### Features

* **OAuth:** Feedback message after connecting oAuth ([ae1afa4](https://gitlab.com/ensembl/dev1on1/commit/ae1afa4f14b23af656e6aa1026d75f613aa0bed0))

### [1.11.1](https://gitlab.com/ensembl/dev1on1/compare/v1.11.0...v1.11.1) (2021-07-10)

## [1.11.0](https://gitlab.com/ensembl/dev1on1/compare/v1.10.0...v1.11.0) (2021-07-09)


### Features

* Update survey design ([423a38f](https://gitlab.com/ensembl/dev1on1/commit/423a38f23cf720571412da1a24c52947962d5572))

## [1.10.0](https://gitlab.com/ensembl/dev1on1/compare/v1.9.2...v1.10.0) (2021-07-09)


### Features

* **oAuth:** Store authedUser at top level doc ([8b90697](https://gitlab.com/ensembl/dev1on1/commit/8b90697c3694edca138126520aaa14c33b2509ab))

### [1.9.2](https://gitlab.com/ensembl/dev1on1/compare/v1.9.0...v1.9.2) (2021-07-09)

### [1.9.1](https://gitlab.com/ensembl/dev1on1/compare/v1.9.0...v1.9.1) (2021-07-09)

## [1.9.0](https://gitlab.com/ensembl/dev1on1/compare/v1.8.2...v1.9.0) (2021-07-09)


### Features

* **oAuth:** Get client id from environment config ([92a7303](https://gitlab.com/ensembl/dev1on1/commit/92a73033c190ef84969c009a390497233023924e))

### [1.8.2](https://gitlab.com/ensembl/dev1on1/compare/v1.8.1...v1.8.2) (2021-07-09)

### [1.8.1](https://gitlab.com/ensembl/dev1on1/compare/v1.8.0...v1.8.1) (2021-07-09)

## [1.8.0](https://gitlab.com/ensembl/dev1on1/compare/v1.7.0...v1.8.0) (2021-07-09)


### Features

* **OAuth flow:** Success and failure messages ([77a915a](https://gitlab.com/ensembl/dev1on1/commit/77a915a771fc36f14e6e168f8d2b16cf3b7a615b))

## [1.7.0](https://gitlab.com/ensembl/dev1on1/compare/v1.6.0...v1.7.0) (2021-07-09)


### Features

* **Sign in flow:** Update wording/UI for sign in flow ([25e3f7a](https://gitlab.com/ensembl/dev1on1/commit/25e3f7abdb39f0f1a8f4e87c8258d7b6c5851174))

## [1.6.0](https://gitlab.com/ensembl/dev1on1/compare/v1.5.2...v1.6.0) (2021-07-09)


### Features

* **Slash command:** Update wording of slash command dm warning ([92d362d](https://gitlab.com/ensembl/dev1on1/commit/92d362d1d7f80812747817a6018ff774550c3791))
* Show empty message when no report available ([eebe1de](https://gitlab.com/ensembl/dev1on1/commit/eebe1de21f15d1068e682f89fde750ba4fbefc23))

### [1.5.2](https://gitlab.com/ensembl/dev1on1/compare/v1.5.1...v1.5.2) (2021-07-07)


### Bug Fixes

* Fix botapp import ([a2fe639](https://gitlab.com/ensembl/dev1on1/commit/a2fe63911e34442ccfe66344b9b5cb731663e542))

### [1.5.1](https://gitlab.com/ensembl/dev1on1/compare/v1.5.0...v1.5.1) (2021-07-07)

## [1.5.0](https://gitlab.com/ensembl/dev1on1/compare/v1.4.0...v1.5.0) (2021-07-07)


### Features

* Add cloud function to handle oauth workflow ([32117c1](https://gitlab.com/ensembl/dev1on1/commit/32117c1b82ef59571cee394f326c27ef1cb6f61f))
* Add createdAt field ([10979a1](https://gitlab.com/ensembl/dev1on1/commit/10979a106544a358b9cd764ceb2677f7340c0b67))
* Allow respond in DM ([1c86b52](https://gitlab.com/ensembl/dev1on1/commit/1c86b523a45d0f8df3d664065fcf5963ebfcb589))
* Automatically invite other person in DM ([a35bd97](https://gitlab.com/ensembl/dev1on1/commit/a35bd97501d4bf75cab5c340ef6550d74c8d5a28))
* Create function to send reminder to bolt app ([9f06da6](https://gitlab.com/ensembl/dev1on1/commit/9f06da610bd7a33e7e0c4ad67d45f9dd829fce76))
* Create report cmd and generate report ([0bb243d](https://gitlab.com/ensembl/dev1on1/commit/0bb243de9ee1699c86beb63a517fbb0891c48015))
* Create take-survey button ([6dab380](https://gitlab.com/ensembl/dev1on1/commit/6dab3807230faab7cb4e9e132f8ccd08b329a53b))
* Generate a survey button ([f69d5a7](https://gitlab.com/ensembl/dev1on1/commit/f69d5a70f45030c680ef8f590395bcc46eb00280))
* Get manager from DM channel ([36ecb43](https://gitlab.com/ensembl/dev1on1/commit/36ecb4377c90644337835438c49ceb75c112fddd))
* Look up user token in database ([4ef2152](https://gitlab.com/ensembl/dev1on1/commit/4ef2152f326a82922599c18d4693bd9cada4d1b1))
* Only allow slash command in direct message ([97b4c36](https://gitlab.com/ensembl/dev1on1/commit/97b4c368138532164e26d68c0c1228436a21940b))
* Query report based on channel id ([212f7e0](https://gitlab.com/ensembl/dev1on1/commit/212f7e0b04753a551c11eb3a389cba68e9584f95))
* Respond to submitted survey ([d71b00b](https://gitlab.com/ensembl/dev1on1/commit/d71b00b60dfd903e3f0ee49c478758ed18d2a0e5))
* Response to survey button click ([3bf07a9](https://gitlab.com/ensembl/dev1on1/commit/3bf07a90f72db8f34eec89642008ebe50bc09bd0))
* Send periodic notification ([290d919](https://gitlab.com/ensembl/dev1on1/commit/290d91924909fc128c701aa9c7bb6c25d46cce7b))
* Show auth button if user is not in Firestore ([ba76429](https://gitlab.com/ensembl/dev1on1/commit/ba764299d26d62dbdb4a2475df904d41d4e62962))
* Store channel id in survey response ([a5b4873](https://gitlab.com/ensembl/dev1on1/commit/a5b4873bf4f318e3f9fdd1fcdceef448e4c3ed96))
* Store survey res in Firestore ([8ccad42](https://gitlab.com/ensembl/dev1on1/commit/8ccad42395e739a1d8ed65be3d56d05091abcf30))
* Store timestamp for invite and survey completion ([93a8f43](https://gitlab.com/ensembl/dev1on1/commit/93a8f4393cb7048f2db8537d7fcf3137e9939913))
* Store user token in Firestore ([a5a4510](https://gitlab.com/ensembl/dev1on1/commit/a5a4510e7b248b211e6106147a27f2d0cfd55495))


### Bug Fixes

* Fix unathorized request not being ack ([5862b75](https://gitlab.com/ensembl/dev1on1/commit/5862b7510bdd56d05bfab70885a249478cbbf754))

## [1.4.0](https://gitlab.com/ensembl/dev1on1/compare/v1.3.0...v1.4.0) (2021-06-30)


### Features

* Open up a modal for survey ([c8aa62a](https://gitlab.com/ensembl/dev1on1/commit/c8aa62afd24fff7d8ea7e96c6a3f9a94a2a56875))

## [1.3.0](https://gitlab.com/ensembl/dev1on1/compare/v1.2.0...v1.3.0) (2021-06-30)


### Features

* Generate a placeholder response on slash cmd ([d7b4121](https://gitlab.com/ensembl/dev1on1/commit/d7b4121706461ee16b564078ff284bbcde616083))

## [1.2.0](https://gitlab.com/ensembl/dev1on1/compare/v1.1.1...v1.2.0) (2021-06-30)


### Features

* Getting up the basic for Slack app ([a54dc37](https://gitlab.com/ensembl/dev1on1/commit/a54dc37d20aa756b1f290737625eac058d5b238e))

### [1.1.1](https://gitlab.com/ensembl/dev1on1/compare/v1.1.0...v1.1.1) (2021-06-30)

## [1.1.0](https://gitlab.com/ensembl/dev1on1/compare/v1.0.1...v1.1.0) (2021-06-30)


### Features

* **lint:** Make lint format same as Ensembl Talk project ([51ebb95](https://gitlab.com/ensembl/dev1on1/commit/51ebb959e631edddf8b852ce2905a70821d2631c))

### 1.0.1 (2021-06-29)
