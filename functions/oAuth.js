const functions = require('firebase-functions');
const axios = require('axios');

const { admin } = require('./admin');
const { botApp } = require('./botApp');
const { onboardingMessage } = require('./templates/onboardingMessage');

const { client: botClient } = botApp;
const { client_id: slackClientId, client_secret: slackClientSecret } = functions.config().slack;
const db = admin.firestore();

const slackOAuthEndpoint = 'https://slack.com/api/oauth.v2.access';
const errorMessage = 'Something went wrong. Close this window and try again in Slack.';
const successMessage = 'Signed in successfully to Dev 1on1. Close this window, return to Slack, and perform you previous action again.';

exports.oAuth = functions.https.onRequest(async (req, res) => {
  const { code } = req.query;

  let oAuthRes;
  try {
    oAuthRes = await axios.post(slackOAuthEndpoint, `client_id=${slackClientId}&client_secret=${slackClientSecret}&code=${code}`);
  } catch (error) {
    functions.logger.error('Error posting to slackOAuthEndpoint', {
      slackOAuthEndpoint, slackClientId, slackClientSecret, code,
    });
    return res.status(400).send(errorMessage);
  }

  const { authed_user: slackAuthedUser, team } = oAuthRes.data;
  const { id: teamId } = team;
  const { id: slackUserId } = slackAuthedUser;

  try {
    await db.collection('teams').doc(teamId).set({ userLastSignedIn: new Date() }, { merge: true });
  } catch (error) {
    functions.logger.error('Error setting team', { teamId });
  }

  try {
    await db.collection(`teams/${teamId}/users`).doc(slackUserId).set({
      createdAt: new Date(),
      slackAuthedUser,
    });
  } catch (error) {
    functions.logger.error('Error storing slackAuthedUser', { slackUserId });
    return res.status(400).send(errorMessage);
  }

  try {
    await botClient.chat.postMessage({
      channel: slackUserId,
      blocks: onboardingMessage,
    });
  } catch (error) {
    functions.logger.error('Error posting onboarding message', { error, slackUserId });
    return res.status(400).send(errorMessage);
  }

  return res.status(200).send(successMessage);
});
