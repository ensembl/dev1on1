const dayjs = require('dayjs');
const localizedFormat = require('dayjs/plugin/localizedFormat');

const { admin } = require('./admin');
const { noSurveySubmissionsMessage } = require('./templates/noSurveySubmissionsMessage');
const { reportView } = require('./templates/report');

dayjs.extend(localizedFormat);
const db = admin.firestore();

const ic = async ({ teamId, channelId }) => {
  const channelSnapshot = await db.collection(`teams/${teamId}/channels/`).doc(channelId).get();
  const { icId, icName } = channelSnapshot.data();
  return { icId, icName };
};

const answers = async ({ icId, teamId, channelId }) => {
  const q1Answers = [];
  const q2Answers = [];
  const q3Answers = [];
  const snapshots = await db
    .collection(`teams/${teamId}/channels/${channelId}/surveySubmissions`)
    .where('createdById', '==', icId)
    .orderBy('createdAt')
    .get();
  snapshots.docs.forEach((doc) => {
    const {
      question1Answer, question2Answer, question3Answer, createdAt,
    } = doc.data();
    q1Answers.push({ value: question1Answer, createdAt });
    q2Answers.push({ value: question2Answer, createdAt });
    q3Answers.push({ value: question3Answer, createdAt });
  });
  return { q1Answers, q2Answers, q3Answers };
};

exports.report = async ({ ack, body, respond }) => {
  await ack();
  const { team_id: teamId, channel_id: channelId, user_id: currentUserId } = body;
  const { icId, icName } = await ic({ teamId, channelId });
  const {
    q1Answers, q2Answers, q3Answers,
  } = await answers({ icId, teamId, channelId });
  await respond(
    q1Answers.length === 0
      ? noSurveySubmissionsMessage({ icName, isIc: currentUserId === icId })
      : reportView({
        icName, q1Answers, q2Answers, q3Answers,
      }),
  );
};
