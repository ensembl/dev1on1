const { setupChannelMessage } = require('./templates/setupChannelMessage');
const { admin } = require('./admin');
const { setup } = require('./setup');

const db = admin.firestore();

exports.checkChannelSetup = async ({
  ack, client, next, payload, respond,
}) => {
  const { team_id: teamId, channel_id: channelId } = payload;
  const channelSnapshot = await db.collection(`teams/${teamId}/channels`).doc(channelId).get();
  if (channelSnapshot.exists) {
    await next();
  } else {
    await respond(setupChannelMessage);
    await setup({
      ack, client, payload, respond,
    });
  }
};
