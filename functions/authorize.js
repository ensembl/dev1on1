const functions = require('firebase-functions');
const { admin } = require('./admin');

const db = admin.firestore();

exports.authorize = async (input) => {
  let userDoc;
  const { userId, teamId } = input;

  try {
    userDoc = await db.collection(`teams/${teamId}/users`).doc(userId).get();
  } catch (error) {
    functions.logger.error('Error getting user doc', { input });
  }
  return {
    userToken: userDoc.exists
      ? userDoc.data().slackAuthedUser.access_token
      : null,
    botId: 'B1234', // Not exactly sure what the botId is for right now but it's required otherwise it doesn't work
  };
};
