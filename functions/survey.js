const functions = require('firebase-functions');

const { surveyModal } = require('./templates/surveyModal');
const { surveySubmittedModal } = require('./templates/surveySubmittedModal');
const { admin } = require('./admin');

const db = admin.firestore();

exports.surveyOnView = async ({
  ack, body, client, payload,
}) => {
  await ack();
  surveyModal.private_metadata = payload.channel_id;
  await client.views.open({
    trigger_id: body.trigger_id,
    view: surveyModal,
  });
};

exports.surveyOnSubmit = async ({
  ack, body, payload, view,
}) => {
  const { team_id: teamId, state } = view;
  const { values } = state;
  const { private_metadata: channelId } = payload;
  try {
    await db.collection(`teams/${teamId}/channels/${channelId}/surveySubmissions`).add({
      createdById: body.user.id,
      createdAt: new Date(),
      question1Answer: values.question_1['question_1-action'].selected_option.value,
      question2Answer: values.question_2['question_2-action'].selected_option.value,
      question3Answer: values.question_3['question_3-action'].value,
    });
  } catch (error) {
    functions.logger.error('Error adding survey submission', { error });
    return;
  }
  await ack({
    response_action: 'update',
    view: surveySubmittedModal,
  });
};
