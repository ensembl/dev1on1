const admin = require('firebase-admin');
const serviceAccount = require('./firebase-adminsdk-token.json');

const settings = {
  projectId: 'ensembl-dev1on1',
};

const useEmulator = false;

if (process.env.FUNCTIONS_EMULATOR || useEmulator) {
  settings.credential = admin.credential.cert(serviceAccount);
  process.env.FIRESTORE_EMULATOR_HOST = 'localhost:8080';
}

admin.initializeApp(settings);

module.exports = {
  admin,
};
