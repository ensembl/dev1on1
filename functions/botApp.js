const { App } = require('@slack/bolt');
const functions = require('firebase-functions');

const config = functions.config();

exports.botApp = new App({
  signingSecret: config.slack.signing_secret,
  clientId: config.slack.client_id,
  processBeforeResponse: true,
  token: config.slack.bot_token,
});
