const functions = require('firebase-functions');
const { App, ExpressReceiver } = require('@slack/bolt');
const { authorize } = require('./authorize');
const { setup, setRoles } = require('./setup');
const { surveyOnView, surveyOnSubmit } = require('./survey');
const { report } = require('./report');
const { unauthorizeCheck } = require('./unauthorizeCheck');
const { checkIsDM } = require('./checkIsDM');
const { checkChannelSetup } = require('./checkChannelSetup');
const { acked } = require('./acked');

const config = functions.config();

const expressReceiver = new ExpressReceiver({
  signingSecret: config.slack.signing_secret,
  endpoints: '/events',
  processBeforeResponse: true,
});

const app = new App({
  receiver: expressReceiver,
  signingSecret: config.slack.signing_secret,
  clientId: config.slack.client_id,
  processBeforeResponse: true,
  authorize,
});

app.use(unauthorizeCheck);

app.command('/dev1on1-setup', checkIsDM, setup);
app.action('setRoles1', setRoles);
app.action('setRoles2', setRoles);

app.command('/dev1on1-survey', checkIsDM, checkChannelSetup, surveyOnView);
app.command('/dev1on1-report', checkIsDM, checkChannelSetup, report);

app.action('question_1-action', acked);
app.action('question_2-action', acked);
app.action('question_3-action', acked);

app.view('survey_callback_id', surveyOnSubmit);

exports.slackApp = functions.https.onRequest(expressReceiver.app);
