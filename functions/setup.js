const functions = require('firebase-functions');

const { admin } = require('./admin');
const { initialSetupMessage } = require('./templates/initialSetupMessage');
const { changeSetupMessage } = require('./templates/changeSetupMessage');

const db = admin.firestore();

exports.setup = async ({
  ack, client, payload, respond,
}) => {
  await ack();
  const {
    team_id: teamId,
    channel_id: channelId,
  } = payload;
  const channelSnapshot = await db.collection(`teams/${teamId}/channels`).doc(channelId).get();
  if (channelSnapshot.exists) {
    const {
      icId, icName, managerId, managerName,
    } = channelSnapshot.data();
    await respond(
      changeSetupMessage({
        icId, icName, managerId, managerName,
      }),
    );
    return;
  }
  const { members } = await client.conversations.members({ channel: channelId });
  const [user1Id, user2Id] = members;
  const { members: allMembers } = await client.users.list();
  const member1 = allMembers.find(({ id }) => id === user1Id);
  const { real_name: user1Name } = member1.profile;
  const member2 = allMembers.find(({ id }) => id === user2Id);
  const { real_name: user2Name } = member2.profile;
  await respond(
    initialSetupMessage({
      user1Id, user1Name, user2Id, user2Name,
    }),
  );
};

exports.setRoles = async ({
  ack, body, client, payload,
}) => {
  await ack();
  const { team, channel } = body;
  const { id: teamId } = team;
  const { id: channelId } = channel;

  const newRoles = JSON.parse(payload.value);
  const { ic, manager } = newRoles;
  const { id: icId, name: icName } = ic;
  const { id: managerId, name: managerName } = manager;
  try {
    await db.collection(`teams/${teamId}/channels`).doc(channelId).set({
      updatedAt: new Date(),
      icId,
      icName,
      managerId,
      managerName,
    }, { merge: true });
  } catch (error) {
    functions.logger.error('Error writing ic and manager information', {
      icId, icName, managerId, managerName,
    });
    return;
  }
  await client.chat.postMessage({
    channel: channelId,
    text: `*${icName}* is now set as the IC and *${managerName}* as the manager in this DM channel. Change it by running \`/dev1on1-setup\`. Submit a new survey if you're the IC using \`/dev1on1-survey\`\n_Dev 1on1: Hey we're posting on your behalf here since it's a <https://api.slack.com/methods/chat.postMessage#channels__post-to-a-direct-message-channel|limitation of Slack>._ :bow:`,
    unfurl_links: false,
  });
};
