const functions = require('firebase-functions');

const { admin } = require('./admin');
const { botApp } = require('./botApp');
const { surveyReminderMessage } = require('./templates/surveyReminderMessage');

const { client: botClient } = botApp;
const db = admin.firestore();

const notifyAll = async () => {
  let channelSnapshots;
  try {
    channelSnapshots = await db.collectionGroup('channels').get();
  } catch (error) {
    functions.logger.error('Error getting channel snapshots', { error });
    return;
  }
  const icIds = new Set();
  channelSnapshots.docs.forEach((doc) => { icIds.add(doc.data().icId); });
  const icIdsArray = [...icIds];
  for (let i = 0; i < icIdsArray.length; i += 1) {
    try {
      await botClient.chat.postMessage({ // eslint-disable-line no-await-in-loop
        channel: icIdsArray[i],
        blocks: surveyReminderMessage,
      });
    } catch (error) {
      functions.logger.error('Error posting reminder notification', { error });
    }
  }
};

exports.remind = functions.pubsub.schedule('0 15 * * 2,4').onRun(async () => {
  functions.logger.info('surveyReminder-remind fired');
  notifyAll();
});

// For local development: http://localhost:5001/ensembl-dev1on1/us-central1/surveyReminder-devRemind
exports.devRemind = functions.https.onRequest(async (req, res) => {
  functions.logger.info('surveyReminder-devRemind fired');
  await notifyAll();
  return res.status(200).send('Success');
});
