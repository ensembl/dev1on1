const { useSlashCommandInDMMessage } = require('./templates/useSlashCommandInDMMessage');

exports.checkIsDM = async ({
  payload, ack, next, respond,
}) => {
  if (payload.channel_name === 'directmessage') {
    await next();
  } else {
    await ack();
    await respond(useSlashCommandInDMMessage);
  }
};
