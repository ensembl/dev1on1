const { signInButton } = require('./templates/signInButton');

exports.unauthorizeCheck = async ({
  context, next, respond, ack,
}) => {
  if (!context.userToken) {
    await ack();
    await respond(signInButton);
  } else {
    await next();
  }
};
