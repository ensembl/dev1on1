exports.surveySubmittedModal = {
  type: 'modal',
  title: {
    type: 'plain_text',
    text: 'Survey submitted',
  },
  close: {
    type: 'plain_text',
    text: 'Dismiss',
    emoji: true,
  },
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: 'Survey submitted successfully. Run `/dev1on1-report` in the DM channel with your manager to see aggregated submission data.',
      },
    },
  ],
};
