exports.surveyModal = {
  type: 'modal',
  callback_id: 'survey_callback_id',
  title: {
    type: 'plain_text',
    text: 'Submit survey',
    emoji: true,
  },
  submit: {
    type: 'plain_text',
    text: 'Submit',
    emoji: true,
  },
  close: {
    type: 'plain_text',
    text: 'Cancel',
    emoji: true,
  },
  blocks: [
    {
      type: 'input',
      block_id: 'question_1',
      element: {
        type: 'radio_buttons',
        options: [
          {
            text: {
              type: 'mrkdwn',
              text: ':thumbsup: Not really',
            },
            value: '1',
          },
          {
            text: {
              type: 'mrkdwn',
              text: ':thumbsup: '.repeat(2),
            },
            value: '2',
          },
          {
            text: {
              type: 'mrkdwn',
              text: ':thumbsup: '.repeat(3),
            },
            value: '3',
          },
          {
            text: {
              type: 'mrkdwn',
              text: `${':thumbsup: '.repeat(4)} Very much`,
            },
            value: '4',
          },
        ],
        action_id: 'question_1-action',
      },
      label: {
        type: 'plain_text',
        text: 'Do you feel your contributions are being recognized by your team right now?',
        emoji: true,
      },
    },
    {
      type: 'input',
      block_id: 'question_2',
      element: {
        type: 'radio_buttons',
        options: [
          {
            text: {
              type: 'mrkdwn',
              text: ':thumbsup: Not really motivated',
            },
            value: '1',
          },
          {
            text: {
              type: 'mrkdwn',
              text: ':thumbsup: '.repeat(2),
            },
            value: '2',
          },
          {
            text: {
              type: 'mrkdwn',
              text: ':thumbsup: '.repeat(3),
            },
            value: '3',
          },
          {
            text: {
              type: 'mrkdwn',
              text: `${':thumbsup: '.repeat(4)} Very much motivated`,
            },
            value: '4',
          },
        ],
        action_id: 'question_2-action',
      },
      label: {
        type: 'plain_text',
        text: 'How motivated are you feeling right now?',
        emoji: true,
      },
    },
    {
      type: 'input',
      block_id: 'question_3',
      element: {
        type: 'plain_text_input',
        action_id: 'question_3-action',
        multiline: true,
      },
      label: {
        type: 'plain_text',
        text: 'Other feedback',
        emoji: true,
      },
    },
  ],
};
