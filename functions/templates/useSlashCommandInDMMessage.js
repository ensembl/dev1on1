exports.useSlashCommandInDMMessage = {
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: 'Use *Dev 1on1* slash commands in a DM channel with your manager or your direct report IC (individual contributor). Try again in a DM channel.',
      },
    },
  ],
};
