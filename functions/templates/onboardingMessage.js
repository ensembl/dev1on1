exports.onboardingMessage = [
  {
    type: 'header',
    text: {
      type: 'plain_text',
      text: ':tada: Welcome to Dev 1on1 :tada:',
      emoji: true,
    },
  },
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: 'Dev 1on1 helps you have better 1 on 1 meetings with your engineering manager or your developer direct report IC (individual contributor). Dev 1on1 works with slash commands. Run `/dev1on1-setup` in a DM channel with your manager or your IC to get started.',
    },
  },
  {
    type: 'divider',
  },
  {
    type: 'section',
    fields: [
      {
        type: 'mrkdwn',
        text: '`/dev1on1-setup`',
      },
      {
        type: 'mrkdwn',
        text: 'Set up who is the manager and who is the IC in a DM channel.',
      },
    ],
  },
  {
    type: 'section',
    fields: [
      {
        type: 'mrkdwn',
        text: '`/dev1on1-survey`',
      },
      {
        type: 'mrkdwn',
        text: 'Submit a survey as an IC in a DM channel. Run this several times a week.',
      },
    ],
  },
  {
    type: 'section',
    fields: [
      {
        type: 'mrkdwn',
        text: '`/dev1on1-report`',
      },
      {
        type: 'mrkdwn',
        text: 'Show a report of aggregated survey data. Use this during your 1 on 1 meeting.',
      },
    ],
  },
];
