const functions = require('firebase-functions');

const slackClientId = functions.config().slack.client_id;
exports.signInButton = {
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: 'Sign in to *Dev 1on1* with your Slack account to perform this action.',
      },
    },
    {
      type: 'actions',
      elements: [
        {
          type: 'button',
          text: {
            type: 'plain_text',
            text: 'Sign in',
            emoji: true,
          },
          value: 'sign_in',
          url: `https://slack.com/oauth/v2/authorize?client_id=${slackClientId}&scope=app_mentions:read,chat:write,chat:write.customize,chat:write.public,commands,im:history,im:read,im:write,mpim:write,mpim:read,mpim:history&user_scope=channels:read,channels:write,chat:write,im:read,users:read`,
          style: 'primary',
        },
      ],
    },
  ],
};
