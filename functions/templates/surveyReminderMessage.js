exports.surveyReminderMessage = [
  {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: 'Friendly reminder :blush:\nRun `/dev1on1-survey` in the DM channel with your manager to submit an updated survey. It\'ll help your next 1 on 1 meeting!',
    },
  },
];
