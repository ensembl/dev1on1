const dayjs = require('dayjs');
const localizedFormat = require('dayjs/plugin/localizedFormat');

dayjs.extend(localizedFormat);

exports.reportView = ({
  icName, q1Answers, q2Answers, q3Answers,
}) => {
  const titleBlock = [{
    type: 'header',
    text: {
      type: 'plain_text',
      text: `Report for ${icName}`,
      emoji: true,
    },
  }];

  const q1HeaderBlock = [{
    type: 'header',
    text: {
      type: 'plain_text',
      text: 'Do you feel your contributions are being recognized by your team right now?',
      emoji: true,
    },
  }];

  const q1AnswerBlocks = [];
  q1Answers.forEach(({ value, createdAt }) => {
    q1AnswerBlocks.push({
      type: 'section',
      fields: [
        {
          type: 'mrkdwn',
          text: `*${dayjs.unix(createdAt.seconds).format('ll')}*`,
        },
        {
          type: 'mrkdwn',
          text: ':thumbsup: '.repeat(value),
        },
      ],
    });
  });

  const q2HeaderBlock = [{
    type: 'header',
    text: {
      type: 'plain_text',
      text: 'How motivated are you feeling right now?',
      emoji: true,
    },
  }];

  const q2AnswerBlocks = [];
  q2Answers.forEach(({ value, createdAt }) => {
    q2AnswerBlocks.push({
      type: 'section',
      fields: [
        {
          type: 'mrkdwn',
          text: `*${dayjs.unix(createdAt.seconds).format('ll')}*`,
        },
        {
          type: 'mrkdwn',
          text: ':thumbsup: '.repeat(value),
        },
      ],
    });
  });

  const q3HeaderBlock = [{
    type: 'header',
    text: {
      type: 'plain_text',
      text: 'Recent feedback',
      emoji: true,
    },
  }];

  const q3AnswerBlocks = [];
  q3Answers.reverse().forEach(({ value, createdAt }) => {
    q3AnswerBlocks.push({
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `*${dayjs.unix(createdAt.seconds).format('ll')}*\n${value}`,
      },
    });
  });

  return {
    blocks: [
      ...titleBlock,
      ...q1HeaderBlock,
      ...q1AnswerBlocks,
      ...q2HeaderBlock,
      ...q2AnswerBlocks,
      ...q3HeaderBlock,
      ...q3AnswerBlocks,
    ],
  };
};
