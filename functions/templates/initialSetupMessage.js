exports.initialSetupMessage = ({
  user1Id, user1Name, user2Id, user2Name,
}) => {
  const newRoles1 = {
    ic: {
      id: user1Id,
      name: user1Name,
    },
    manager: {
      id: user2Id,
      name: user2Name,
    },
  };
  const newRoles2 = {
    ic: {
      id: user2Id,
      name: user2Name,
    },
    manager: {
      id: user1Id,
      name: user1Name,
    },
  };
  return {
    blocks: [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: 'Set up *Dev 1on1* in this DM channel by setting who should be the IC (individual contributor). Set it to be:',
        },
      },
      {
        type: 'actions',
        elements: [
          {
            type: 'button',
            text: {
              type: 'plain_text',
              text: `${user1Name}`,
              emoji: true,
            },
            action_id: 'setRoles1',
            value: JSON.stringify(newRoles1),
          },
          {
            type: 'button',
            text: {
              type: 'plain_text',
              text: `${user2Name}`,
              emoji: true,
            },
            action_id: 'setRoles2',
            value: JSON.stringify(newRoles2),
          },
        ],
      },
    ],
  };
};
