exports.setupChannelMessage = {
  blocks: [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: 'Set up *Dev 1on1* before performing this action.',
      },
    },
  ],
};
