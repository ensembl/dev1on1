exports.noSurveySubmissionsMessage = ({ icName, isIc }) => {
  const text = isIc
    ? 'No survey submissions yet to generate a report. Run `/dev1on1-survey` to submit a survey.'
    : `No survey submissions yet from *${icName}* to generate a report. Ask *${icName}* to run \`/dev1on1-survey\` to submit a survey.`;
  return {
    blocks: [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text,
        },
      },
    ],
  };
};
