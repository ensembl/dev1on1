exports.changeSetupMessage = ({
  icId, icName, managerId, managerName,
}) => {
  const newRoles = {
    ic: {
      id: managerId,
      name: managerName,
    },
    manager: {
      id: icId,
      name: icName,
    },
  };
  return {
    blocks: [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `*${icName}* is set as the IC (individual contributor) in this DM channel.`,
        },
      },
      {
        type: 'actions',
        elements: [
          {
            type: 'button',
            text: {
              type: 'plain_text',
              text: `Change it to be ${managerName}`,
              emoji: true,
            },
            action_id: 'setRoles1',
            value: JSON.stringify(newRoles),
          },
        ],
      },
    ],
  };
};
